The dav1d AV1 decoder uses the [Meson build system][meson]. To compile dav1d, **Meson 0.49 or higher** and [Ninja][ninja] are required.

## Basic compilation

### Linux native compilation

Check out the source code using git:

```
git clone https://code.videolan.org/videolan/dav1d.git
```

Change to the dav1d directory and configure the build with meson:

```
cd dav1d
meson build
```

To compile it, change to the `build` directory and run ninja:

```
cd build
ninja
```

### Windows native compilation

Install [msys2](https://www.msys2.org/), follow the instructions on the page.

Open a mingw64 msys2 shell and install the required packages:

```
pacman -S git mingw-w64-x86_64-meson mingw-w64-x86_64-ninja gcc nasm
```

Then follow the [Linux native compilation](https://code.videolan.org/videolan/dav1d/-/wikis/Compilation-Guide#linux-native-compilation) steps.

## Run tests

To run the tests, such as checkasm, use the meson test subcommand from the build dir:

```
meson test -v
```

## Compilation options

The dav1d build has some options to change aspects of the build, additionally meson provides a number of convenient options too. These options can either be set on an already configure build by invoking `meson configure -Doption_name=value` in the build dir or by passing them on the initial meson command that configures the build dir.

Running `meson configure` in the build dir shows the current configuration state. After changing a value, meson will automatically reconfigure as necessary on next ninja invocation.

Some useful meson options:

- `buildtype`: Change the build type (`debug`, `debugoptimized`, `release`)
- `prefix`: Change the prefix where dav1d will be installed
- `b_sanitize`: Enable a sanitizer (`none`, `address`, `thread`, `undefined`, `memory`)
- `default_library`: Change if a `shared` or `static` library should be built or `both`
- `c_args=...` and `c_link_args=...` to pass custom `CFLAGS`/`LDFLAGS`.

[meson]: https://mesonbuild.com
[ninja]: https://ninja-build.org